//
//  CustomHeader.swift
//  ScalingHeader
//
//  Created by aegunov on 09.11.14.
//  Copyright (c) 2014 Yandex LLC. All rights reserved.
//

import Foundation
import UIKit

class CustomHeaderLoader: UIView {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override init() {
        super.init(frame: CGRectMake(0, 0, 0, 0))
    }
    
    @IBOutlet var header: CustomHeader!
}

class CustomHeader: UIView {
    @IBOutlet weak var shadingView: UIView!
    @IBOutlet weak var priceLabelView: UILabel!
    @IBOutlet weak var desiredHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var horizontalAlignConstraint: NSLayoutConstraint!
    @IBOutlet weak var prefixHorizontalAlignConstraint: NSLayoutConstraint!
    @IBOutlet weak var prefixLabel: UILabel!
    @IBOutlet weak var infoLabelVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var priceVerticalConstraint: NSLayoutConstraint!

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    var prefixText: String? {
        get {
            return prefixLabel.text
        }
        set (value) {
            prefixLabel.text = value
            updateWindowRect()
        }
    }
    
    var priceText: String? {
        get {
            return priceLabelView.text
        }
        set (value) {
            priceLabelView.text = value
            updateWindowRect()
        }
    }
    
    func updateWindowRect() {
        let maxPriceWidth = (self.bounds.width - prefixLabel.bounds.width - 20 - 20 - 20) / 2
        let myString: NSString = priceLabelView.text as NSString!
        var font = priceLabelView.font.fontWithSize(60.0)
        while true {
            let size: CGSize = myString.sizeWithAttributes([NSFontAttributeName: font])
            if size.width < maxPriceWidth {
                break
            }
            font = UIFont(name: font.fontName, size: font.pointSize - 0.01)!
        }
        priceLabelView.font = font
        priceLabelView.sizeToFit()
        applyTransform()
    }

    func applyTransform() {
        var scale: CGFloat = desiredHeightConstraint.constant / defaultHeight
        var a = -priceLabelView.font.descender / priceLabelView.bounds.height
        priceLabelView.layer.anchorPoint = CGPointMake(0.5, 1.0 - a)
        priceLabelView.transform = CGAffineTransformConcat(
            CGAffineTransformMakeScale(scale, scale),
            CGAffineTransformMakeTranslation(0, priceLabelView.bounds.height / 2 - priceLabelView.bounds.height * a)
        )
        let x = (priceLabelView.bounds.width - priceLabelView.bounds.width * scale) / 2.0
        horizontalAlignConstraint.constant = 10.0 - x
        prefixHorizontalAlignConstraint.constant = 10.0 - x

        let size: CGSize = (priceLabelView.text as NSString!).sizeWithAttributes([NSFontAttributeName: priceLabelView.font])
        infoLabelVerticalConstraint.constant = priceVerticalConstraint.constant + size.height * 0.8 * scale - (1 - scale) * size.height / 3
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    var desiredHeight: CGFloat {
        get {
            return desiredHeightConstraint.constant
        }
        set (value) {
            desiredHeightConstraint.constant = value
            applyTransform()
        }
    }

    var defaultHeight: CGFloat {
        return 100.0
    }
}
